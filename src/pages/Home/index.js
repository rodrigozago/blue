import React from 'react';
import Row from 'react-bootstrap/Row';
import CurrencyInput from '../../components/CurrencyInput';
import {
    Typography,
    Slider,
    Button
} from '@material-ui/core';
import {
    HomeContainer,
    LoanFormContainer,
    LoanDetailsCol,
    Red,
    Price,
    DetailsCard,
    PerMonth,
    DetailsCardFooter,
    HeaderLine,
    Months,
    CardText,
} from './styles';

export default function Home() {
    const [loanPrice, setLoanPrice] = React.useState('100000,00');
    const [buildingPrice, setBuildingPrice] = React.useState('250000,00');
    const [sliderValue, setSliderValue] = React.useState(1);
    const [totalValue, setTotalValue] = React.useState(0);

    React.useEffect(() => {
        calcLoan();
    }, [loanPrice, buildingPrice, sliderValue]);

    const onChangeLoan = (event) => {
        setLoanPrice(event.target.value);
        
    };

    const onChangeBuildingPrice = (event) => {
        setBuildingPrice(event.target.value);
        
    };

    const handleSliderChange = (event, newValue) => {
        setSliderValue(newValue);
    };

    const calcLoan = () => {
        // User regular expression to remove thousand separator and replace , with .
        let loanFloatPrice = parseFloat(loanPrice.replace(/[^\d\,\-]/g, '').replace(/[\,]/g, '.'));
        let buildingFloatPrice = parseFloat(buildingPrice.replace(/[^\d\,\-]/g, '').replace(/[\,]/g, '.'));
        let totalValue = loanFloatPrice / sliderValue;
        
        if(buildingFloatPrice <= 100000) {
            totalValue += totalValue*0.01;
        } else {
            totalValue += totalValue*0.015;
        }

        setTotalValue(totalValue);
    };

    const renderMonths = () => {
        if(sliderValue == 1)
           return <Months>{sliderValue} mês</Months>;
        return <Months>{sliderValue} meses</Months>;;
     }


    return (
        <HomeContainer>
            <Row>
                <LoanFormContainer xs={12} lg={8}>
                    <Typography id="discrete-slider-always" gutterBottom>
                        <h3>Valor do empréstimo <Red>*</Red></h3>
                    </Typography>
                        {/* <Input value={loanPrice} /> */}
                        <CurrencyInput class="form-control" onChange={onChangeLoan} value={loanPrice}  placeholder="R$ 100.000,00" type="text" />
                    <h3>Valor do Imovel <Red>*</Red></h3>
                        <CurrencyInput class="form-control" onChange={onChangeBuildingPrice} value={buildingPrice} placeholder="R$ 100.000,00" type="text" />
                    <h3>Quantidade de meses <Red>*</Red></h3>
                    <div class="mt-5">
                        <Slider
                            value={ sliderValue }
                            onChange={handleSliderChange}
                            valueLabelDisplay="on"
                            aria-labelledby="input-slider"
                            min={1}
                            max={120}
                        />
                    </div>
                    <div class="d-flex align-items-end">
                        <Button onClick={calcLoan} variant="contained">Simular Empréstimo</Button>
                    </div>
                </LoanFormContainer>

                <LoanDetailsCol xs={12} lg={4}>
                    <DetailsCard>
                        <HeaderLine />
                        <div class="d-flex flex-column align-items-center justify-content-center">
                            <Price>
                                R$ {totalValue.toFixed(2)}
                            </Price>
                            <PerMonth>/mes</PerMonth>
                        </div>
                        <DetailsCardFooter>
                            <div class="d-flex justify-content-center flex-column align-items-center pt-4">
                                {/* <i class="material-icons">event</i>                             */}
                                Prazo para pagamento: 
                                {renderMonths()}
                                <b>Amortização: TABELA PRICE</b>
                                <CardText>
                                    A taxa de juros varia a partir de 0,99% ao mês, dependendo do tipo de imóvel. A parcela apresentada já inclui os custos aproximados com avaliação do imóvel, seguros e custos cartorais
                                </CardText>
                            </div>
                        </DetailsCardFooter>
                    </DetailsCard>
                </LoanDetailsCol>
            </Row>
        </HomeContainer>
    );
}