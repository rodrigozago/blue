import styled from 'styled-components';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';

export const HomeContainer = styled(Container)`
    margin-top: 40px;
`;

export const LoanFormContainer = styled(Col)`
    height: 400px;
`;

export const LoanDetailsCol = styled(Col)`
    height: 400px;
`;

export const HeaderLine = styled.div`
    background: rgb(180,58,58);
    background: linear-gradient(90deg, rgba(180,58,58,1) 0%, rgba(42,132,199,1) 33%, rgba(175,29,253,1) 66%, rgba(252,69,69,1) 100%);
    height: 6px;
    width: 100%;
`;

export const DetailsCard = styled.div`
    -webkit-box-shadow: 0px 0px 10px 1px rgba(0,0,0,0.22); 
    box-shadow: 0px 0px 10px 1px rgba(0,0,0,0.22);

    display: flex;
    flex-direction: column;
    align-items: center;

    height: 100%;
`;

export const DetailsCardFooter = styled.div`
    background-color: #FFF;
    width: 100%;

    
`;

export const Red = styled.span`
    color: red;
`;

export const Price = styled.span`
    font-size: 30px;
    font-weight: bold;
    margin-bottom: -12px;
`;

export const PerMonth = styled.span`
    color: #494949;
`;

export const Months = styled.span`
    font-size: 18px;
`;

export const CardText = styled.p`
    padding: 18px;
    text-align: center;
    color: #666;
`;